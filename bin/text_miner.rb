require 'csv'
require 'logger'
require 'optparse'
require 'ostruct'
require 'twilio-ruby'

require_relative '../lib/text_mining'

include TextMining

def alert(msg_body)
  account_sid = 'ACa07ea1e40fe82f623801a9bd7a76f419'
  auth_token = '54aaab3f5ea59df21b286c31110bb33a'
  client = Twilio::REST::Client.new account_sid, auth_token
  message = client.account.sms.messages.create(:body => msg_body,
  :to => "+12066173488",
  :from => "+14254092238")    
end

options = OpenStruct.new
options.start = false
options.data_file = nil
options.end = false
options.state_file = ''

opts = OptionParser.new do |opts|
  opts.banner = "Usage: text_miner.rb [-b DATAFILE|-e] -s STATEFILE"
  opts.separator ""
  opts.separator "Options:"
  
  opts.on('-b', '--begin DATAFILE', "Begin learning taking input data from DATAFILE") do |df|
    options.start = true
    options.data_file = df
  end

  opts.on('-e', '--end', 'End learning') do
    options.end = true
  end

  opts.on('-s', '--state STATEFILE', "State file name to which learned state will be or has been saved") do |sf|
    options.state_file = sf
  end

  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end

opts.parse!(ARGV)

unless options.state_file
  puts "Please specify the state file.\n" + opts.to_s
  exit
end

if options.start
  tk = Tokenizer.new
  nbc = NaiveBayesClassifier.new
  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG
  nbc.logger = logger

  # Learn and Save
  nbc.begin_incremental_learn
  lineno = -1
  CSV.foreach(options.data_file, headers: true, col_sep: "\t") do |row|
    lineno += 1
    begin
      doc = tk.tokenize(row['Title_BodyMarkdown_Text'])
      di = DocumentInstance.new(doc, row['OpenStatus'])
      nbc.incremental_learn(di)
      if lineno % 1000 == 0
        print '.'
        logger.debug("Processed line number #{lineno}")
      end
    rescue => ex
      logger.warn("Unable to process post id #{row[PostId]}")
      logger.debug(ex.message + "\n" + ex.backtrace.join("\n"))
      print 'x'
    end
  end
  puts
  nbc.pause_learn(options.state_file)
end

if options.end
  # Load and End
  nbc = NaiveBayesClassifier.new
  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG
  nbc.logger = logger

  nbc.resume_learn(options.state_file)
  nbc.chop_words
  nbc.end_incremental_learn
  tpm = nbc.model
  nbc.clear

  tpm.calculate_info_gains
  head_info_gains = chop_tail(tpm.info_gains)
  CSV.open("info_gain.tsv", "wt", col_sep: "\t") do |tsv|
    head_info_gains.each do |ig|
      tsv << [ig.feature, ig.category, ig.value]
    end
  end
end