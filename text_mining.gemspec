# -*- encoding: utf-8 -*-
require File.expand_path('../lib/text_mining/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Avilay Parekh"]
  gem.email         = ["avilay@gmail.com"]
  gem.description   = <<-DESC
    tokenizer = Tokenizer.new
    doc = tokenizer.tokenize("hello world")
  DESC
  gem.summary       = %q{A set of utilities that are useful for text mining.}
  gem.homepage      = "http://avilay.info"
  gem.files         = `git ls-files`.split($\)
  #gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "text_mining"
  gem.require_paths = ["lib"]
  gem.version       = TextMining::VERSION
end
