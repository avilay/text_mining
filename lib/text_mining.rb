require_relative "text_mining/document"
require_relative "text_mining/document_instance"
require_relative "text_mining/naive_bayes_classifier"
require_relative "text_mining/probabilities"
require_relative "text_mining/text_probability_model"
require_relative "text_mining/token"
require_relative "text_mining/tokenizer"
require_relative "text_mining/info_gain"
require_relative "text_mining/utils"
