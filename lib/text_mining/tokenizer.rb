require 'logger'
require 'stemmer'
require 'set'
require_relative 'token'
require_relative 'document'

module TextMining  
  class Tokenizer

    attr_writer :logger, :emoticons, :tlds, :stopwords, :use_stemming

    def initialize
      @logger = Logger.new(STDERR)
      @logger.level = Logger::ERROR

      data_dir = File.expand_path(File.dirname(__FILE__) + "/../../data")

      @emoticons = {}
      IO.foreach("#{data_dir}/emoticons.tsv") do |line|
        flds = line.strip.split("\t")
        @emoticons[flds[0]] = flds[1]
      end

      @tlds = {}
      IO.foreach("#{data_dir}/tlds.tsv") do |line|
        flds = line.strip.split("\t")
        @tlds[flds[0]] = flds[1]
      end

      @stopwords = {}
      IO.foreach("#{data_dir}/stopwords.txt") do |line|
        @stopwords[line.strip] = true
      end

      @protocols = {}
      IO.foreach("#{data_dir}/protocols.txt") do |line|
        @protocols[line.strip] = true
      end

      @use_stemming = "yes"
    end

    def configure(params)
      @logger = params[:logger] if params[:logger]
      @emoticons = params[:emoticons] if params[:emoticons]
      @tlds = params[:tlds] if params[:tlds]
      @stopwords = params[:stopwords] if params[:stopwords]
      @use_stemming = params[:use_stemming] if params[:use_stemming]
    end

    def tokenize(original_text)
      @tokens = []
      deconstruct(original_text)
      handle_abbrvs
      handle_internet
      handle_emoticons
      combine_words
      handle_numbers
      handle_punctuation
      reconstruct
      standardize
      Document.new(original_text, @tokens)
    end

    # ****** Debug methods
    def debug_log_tokens(label)
      @logger.debug(label)
      @tokens.each{|t| @logger.debug(t.to_s)}
    end

    def print_tokens(label = "LABEL")
      puts label
      @tokens.each{|t| puts(t.to_s)}
    end

    def debug_log_config(label)
      @logger.debug(label)
      @logger.debug("Number of emoticons is #{@emoticons.count}")
      @logger.debug("Number of tlds is #{@tlds.count}")
      @logger.debug("Number of stopwords is #{@stopwords.count}")
      @logger.debug("Use stemming is #{@use_stemming}")
    end
    # *******

    private
    def exists_and_are_unknown?(range)
      ret = true
      range.each {|x| ret = ret && @tokens[x] && @tokens[x].type == Token::UNKNOWN}
      ret
    end

    def ipnum?(txt)
      begin
        if %r{^\d+$}.match(txt)
          num = Integer(txt)
          (0 <= num && num <= 255)
        else
          false
        end
      rescue
        false
      end
    end

    def deconstruct(raw_text)      
      queue = [raw_text.gsub(%r{\s+}, ' ')]
      while tok = queue.shift
        flds = tok.partition(%r{\W}).reject(&:empty?)
        if flds.length == 1 && flds[0] == tok
          @tokens << Token.new(tok)
        else
          queue.push(*flds)  
        end    
      end      
      debug_log_tokens("After deconstruct")
    end

    def handle_abbrvs
      i = 0
      while (i += 1) < @tokens.length
        next unless exists_and_are_unknown?(i..i)
        curr_val = @tokens[i].value
        prev_val = @tokens[i-1].value
        if curr_val == '.'
          matches = %r{([+-]?[\d\.]+)([[:alpha:]]+)}.match(prev_val)
          if matches
            tok1 = Token.new(matches[1])
            tok2 = Token.new(matches[2])
            @tokens[i-1..i-1] = [tok1, tok2]
          end
        end
      end

      i = -1
      while (i += 1) < @tokens.length
        next unless exists_and_are_unknown?(i..i)
        curr_val = @tokens[i].value
        prev_val = @tokens[i-1].value
        prev_type = @tokens[i-1].type
        if (curr_val == '.' && prev_type == Token::ABBRV) ||
            (curr_val == '.' && %r{^[A-Z]$}.match(prev_val)) ||
            (%r{^[A-Z]$}.match(curr_val) && prev_type == Token::ABBRV)
          @tokens[i-1].type = Token::ABBRV
          @tokens[i].type = Token::ABBRV
        end
      end
      debug_log_tokens("After handle_abbrvs")
    end

    def handle_emoticons
      i = -1
      while (i += 1) < @tokens.length
        next unless exists_and_are_unknown?(i..i)
        (i+10).downto(i+1).each do |j|
          next unless exists_and_are_unknown?(i..j)
          eicon = @tokens[i..j].map {|tok| tok.value}.join('')
          if @emoticons[eicon]
            @tokens[i..j].each {|tok| tok.type = Token::EMOTICON}
          end
        end
      end
      debug_log_tokens("After handle_emoticons")
    end

    def combine_words
      i = 0
      while (i += 1) < @tokens.length - 1
        next unless exists_and_are_unknown?(i-1..i+1)        
        curr = @tokens[i].value
        prev = @tokens[i-1].value
        nxt = @tokens[i+1].value
        if %r{['/+&_-]}.match(curr) && %r{^\w}.match(prev) && %r{^\w}.match(nxt)
          @tokens[i-1..i+1].each{|tok| tok.type = Token::COMBINED}
        end
      end
      debug_log_tokens("After combine_words")
    end

    def handle_internet
      if @tokens.length >= 3
        i = -1
        while (i += 1) < @tokens.length - 2
          if @tokens[i].type == Token::UNKNOWN &&
              @tokens[i+1].type == Token::UNKNOWN &&
              @tokens[i+2].type == Token::UNKNOWN &&
              @tokens[i].value == '<' &&
              @tokens[i+2].value == '>'
            @tokens[i..i+2].each{|tok| tok.type = Token::INTERNET}  
          end
        end
      end
      if @tokens.length >= 4
        i = -1
        while (i += 1) < @tokens.length - 3
          if @tokens[i].type == Token::UNKNOWN &&
              @tokens[i+1].type == Token::UNKNOWN &&
              @tokens[i+2].type == Token::UNKNOWN &&
              @tokens[i+3].type == Token::UNKNOWN &&
              @tokens[i].value == '<' &&
              @tokens[i+1].value == '/' &&
              @tokens[i+3].value == '>'
            @tokens[i..i+3].each{|tok| tok.type = Token::INTERNET}
          end
        end
      end
      if @tokens.length >= 7
        i = 0
        while i < @tokens.length - 6
          if @tokens[i].type == Token::UNKNOWN
            if ipnum?(@tokens[i].value) && @tokens[i+1].value == '.' && 
                ipnum?(@tokens[i+2].value) && @tokens[i+3].value == '.' && 
                ipnum?(@tokens[i+4].value) && @tokens[i+5].value == '.' && 
                ipnum?(@tokens[i+6].value)
              @tokens[i..i+6].each{|tok| tok.type = Token::INTERNET}
            end
          end
          i += 1
        end
      end

      i = 0
      while (i += 1) < @tokens.length - 1
        next unless exists_and_are_unknown?(i-1..i+1)
        curr = @tokens[i].value
        prev = @tokens[i-1].value
        nxt = @tokens[i+1].value
        if curr == '.' && @tlds[nxt.downcase] && %r{^\w}.match(prev)
          @tokens[i-1..i+1].each{|tok| tok.type = Token::INTERNET}
          j = i - 2
          while j >= 0
            if @tokens[j].value != ' ' && @tokens[j].type == Token::UNKNOWN
              @tokens[j].type = Token::INTERNET
              break if @protocols[@tokens[j].value.downcase]
            else
              break
            end
            j -= 1
          end
        end
      end
      debug_log_tokens("After handle_internet")
    end
    
    def handle_numbers
      @tokens.each do |token|
        if token.type == Token::UNKNOWN && %r{^\d+$}.match(token.value)
          token.type = Token::NUMBER
        end
      end

      i = -1
      while (i += 1) < @tokens.length
        next unless @tokens[i].type == Token::UNKNOWN
        
        if @tokens[i].value == '%' && 
            @tokens[i-1] && @tokens[i-1].type == Token::NUMBER
          @tokens[i].type = Token::NUMBER
        end

        if %r{[,\.]}.match(@tokens[i].value) && 
            @tokens[i-1] && @tokens[i-1].type == Token::NUMBER && 
            @tokens[i+1] && @tokens[i+1].type == Token::NUMBER
          @tokens[i].type = Token::NUMBER
        end

        if %r{^\d+[Ee]\d+$}.match(@tokens[i].value) && 
            @tokens[i-1] && @tokens[i-1].value == '.' && @tokens[i-1].type == Token::UNKNOWN &&
            @tokens[i-2] && @tokens[i-2].type == Token::NUMBER
          @tokens[i].type = Token::NUMBER
          @tokens[i-1].type = Token::NUMBER
        end

        if %r{[+-]}.match(@tokens[i].value) && 
            @tokens[i+1] && @tokens[i+1].type == Token::NUMBER
          @tokens[i].type = Token::NUMBER
        end
      end
      debug_log_tokens("After handle_numbers")
    end

    def handle_punctuation
      i = 0
      while i < @tokens.length
        if @tokens[i].type == Token::UNKNOWN && 
            %r{[[:punct:]]}.match(@tokens[i].value)
          @tokens[i].type = Token::PUNCTUATION
        end
        i += 1
      end
      debug_log_tokens("After handle_punctuation")
    end

    def reconstruct
      new_tokens = []
      i = -1
      tok_value = ''
      tok_type = Token::UNKNOWN
      tok_label = ''
      while (i += 1) < @tokens.length
        if (@tokens[i].value == ' ' && @tokens[i].type == Token::UNKNOWN) || 
            @tokens[i].type != tok_type
          if tok_value != ''
            new_token = Token.new(tok_value)
            new_token.type = tok_type
            new_token.label = tok_label
            new_tokens << new_token
          end
          if @tokens[i].value == ' '
            tok_value = ''
          else
            tok_value = @tokens[i].value
          end
          tok_type = @tokens[i].type
          tok_label = @tokens[i].label
        elsif @tokens[i].type == tok_type
          tok_value += @tokens[i].value
        end
      end
      if tok_value != ''
        new_token = Token.new(tok_value)
        new_token.type = tok_type
        new_token.label = tok_label
        new_tokens << new_token
      end
      @tokens = new_tokens
      debug_log_tokens("After reconstruct")
    end

    def standardize
      @tokens.each do |token|
        if token.type == Token::UNKNOWN
          token.label = token.value.downcase
          token.label = token.label.stem if @use_stemming == "yes"
        else
          token.label = token.value
        end
      end
      @tokens.reject! do |token|
        @stopwords[token.label]
      end
      debug_log_tokens("After standardize")
    end

  end
end