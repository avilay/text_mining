module TextMining
  class DocumentInstance

    attr_accessor :document, :category

    def initialize(doc, category)
      self.document = doc
      self.category = category
    end

  end
end