module TextMining

  def chop_tail(elements)
    tot = elements.reduce(0) {|memo, element| memo += element.value}
    avg = tot.to_f/elements.count
    elements.find_all {|element| element.value >= avg}
  end

end