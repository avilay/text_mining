require_relative 'token'

module TextMining
  class Document
    attr_accessor :original_text, :tokens

    def self.unify_docs(docs)
      ud = Document.new(docs[0].original_text, docs[0].tokens)
      docs[1..-1].each {|d| ud.add(d)}
      ud
    end

    def initialize(text, toks)
      @original_text = text
      @tokens = deep_copy_tokens(toks)
    end

    def words
      self.tokens.map {|t| t.label}
    end

    def frequency(word)
      @tokens.find_all {|t| t.label == word}.count
    end

    def add(doc2)
      @original_text += " " + doc2.original_text
      @tokens += deep_copy_tokens(doc2.tokens)
    end

    def to_s
      ret = "Original Text: #{self.original_text}\n"
      ret += "Tokens:\n"
      @tokens.each do |token|
        ret += token.to_s + "\n"
      end
      ret
    end

    private
    def deep_copy_tokens(tokens)
      new_tokens = []
      tokens.each do |token|
        new_tokens << token.clone
      end
      new_tokens
    end

  end
end