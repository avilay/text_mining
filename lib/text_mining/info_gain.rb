module TextMining
  
  class InfoGain
    attr_accessor :feature, :category, :value

    def initialize(params)
      self.feature = params[:feature]
      self.category = params[:category]
      self.value = params[:value]
    end

    def to_h
      {'feature' => self.feature, 'category' => self.category, 'value' => self.value}
    end

    def ==(that)
      self.feature == that.feature &&
      self.category == that.category &&
      (self.value - that.value).abs <= 0.01
    end
  end

end