require 'set'
require 'json'
require_relative 'probabilities'

module TextMining

class TextProbabilityModel
  attr_accessor :probs, :categories, :vocabulary, :entropies, :info_gains
  attr_writer :logger

  def initialize
    @logger = Logger.new(nil)
    @probs = Probabilities.new
    @categories = []
    @vocabulary = Set.new
    @entropies = {}
    @info_gains = []
  end

  def save(filename)
    File.open(filename, 'w') do |file|
      @logger.info("Saving probabilities")
      file.puts(JSON.generate(@probs.to_h))
      @probs.clear

      @logger.info("Saving categories")
      file.puts(JSON.generate(@categories))
      @categories.clear

      @logger.info("Saving vocabulary")
      file.puts(JSON.generate(@vocabulary.to_a))
      @vocabulary.clear

      @logger.info("Saving entropies")
      file.puts(JSON.pretty_generate(@entropies))
      @entropies.clear

      @logger.info("Saving info gains")
      file.puts(JSON.pretty_generate(@info_gains.map{|ig| ig.to_h}))
      @info_gains.clear
    end
  end

  def load(filename)
    lines = IO.readlines(filename)
    @probs = Probabilities.new(JSON.parse(lines[0]))
    @categories = JSON.parse(lines[1])
    @vocabulary = JSON.parse(lines[2])
    @entropies = JSON.parse(lines[3])
    @info_gains = JSON.parse(lines[4])
  end

  def to_s
    ret = "CATEGORIES\n"
    ret += @categories.join(' ') + "\n\n"

    # PROBABILITIES
    ret += @probs.to_s + "\n"

    ret += "VOCABULARY\n"
    ret += @vocabulary.to_a.join(' ') + "\n\n"

    ret += "ENTROPIES\n"
    ret += @entropies.inspect + "\n\n"

    ret += "INFO GAINS\n"
    ret += @info_gains.inspect + "\n\n"

    ret
  end

  def calculate_entropies
    @logger.info("TextProbabilityModel.calculate_entropies") {"Calculating entropies for #{@vocabulary.count} words and #{@categories.length} of categories."}

    # Calculate the entropy of the entire data set. It is calculated as - 
    # - P(CAT1).log2(P(CAT1)) - P(CAT2).log2(P(CAT2)) - ..
    h0 = @categories.reduce(0) do |x, category|
           p = @probs.probability(of: category)
           x += -p * Math.log2(p)           
         end
    @entropies[''] = h0

    @vocabulary.each do |word|
      # Calculate the probability of each word in the vocab as -
      # P(word) = P(CAT1).P(word/CAT1) + P(CAT2).P(word/CAT2) + ...
      p = 0
      @categories.each do |category|
        p += @probs.probability(of: category) * @probs.probability(of: word, given: category)
      end
      @probs.set_probability(of: word, value: p)

      @categories.each do |category|
        # Calculate the probability of each category/word
        # According to Baye's Theorem -
        # P(CAT/word) = {P(CAT).P(word/CAT)}/P(word)
        pc = @probs.probability(of: category)
        pwc = @probs.probability(of: word, given: category)
        pw = @probs.probability(of: word)
        pcw = (pc * pwc)/pw
        @probs.set_probability(of: category, given: word, value: pcw)
      end

      # Entropy per word is calculated as -
      # - P(CAT1/word).log2(P(CAT1/word)) - P(CAT2/word).log2(P(CAT2/word)) - ..
      h = @categories.reduce(0) do |x, category|
            p = @probs.probability(of: category, given: word)
            x += -p * Math.log2(p)
          end
      @entropies[word] = h
    end

    @logger.info("TextProbabilityModel.calculate_entropies") {"Calculated #{entropies.count} number of entropies"}
  end

  def calculate_info_gains
    @logger.info("TextProbabilityModel.calculate_info_gains") {"Calculating info gains"}
    calculate_entropies if @entropies.count == 0
    @vocabulary.each do |word|
      ig = @entropies[''] - @entropies[word]      

      # Find the dominant category for this word
      dom_cat = ''
      max_val = 0
      @categories.each do |category|
        p = @probs.probability(of: word, given: category)
        if p > max_val
          max_val = p
          dom_cat = category
        end
      end
    @info_gains << InfoGain.new(feature: word, category: dom_cat, value: ig)  
    end
    @logger.info("TextProbabilityModel.calculate_entropies"){"Calculated #{info_gains.length} number of info gains."}
  end

end # end class

end #end module