module TextMining

  class Probability
    attr_accessor :of, :given, :value

    # def initialize(params)
    #   self.of = params[:of]
    #   self.given = params[:given]
    #   self.value = params[:value]
    # end

    def initialize(of, given, value)
      self.of = of
      self.given = given
      self.value = value
    end

    def to_h
      {'of' => self.of, 'given' => self.given, 'value' => self.value}
    end

    def to_s
      if self.given
        "P(#{self.of}/#{self.given}) = #{self.value}"
      else
        "P(#{self.of}) = #{self.value}"
      end
    end
  end

  class Probabilities
    def initialize(arr_hash = nil)
      # Probability objects keyed by of+given+word
      @probs = {}
      
      if arr_hash
        arr_hash.each do |hash|
          set_probability(Hash[hash.map{ |k, v| [k.to_sym, v] }])
        end
      end
    end

    def clear
      @probs.clear
    end

    def count
      @probs.count
    end

    def to_h
      probs_arr = []
      @probs.values.each {|p| probs_arr << p.to_h}
      probs_arr
    end

    def probability(params)
      @probs[genkey(params)].value
    end

    def genkey(params)
      params[:of].to_s + params[:given].to_s
    end

    def set_probability(params)
      @probs[genkey(params)] = Probability.new(params[:of], params[:given], params[:value])
    end

    def to_s
      str = "PROBABILITIES: \n"
      @probs.values.each {|p| str += p.to_s + "\n"}
      str
    end
  end

end