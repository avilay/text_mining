module TextMining
	class Token
    ABBRV = "A"
    WORD = "W"
    EMOTICON = "E"
    INTERNET = "I"
    NUMBER = "N"
    PUNCTUATION = "P"
    UNKNOWN = "U"
    COMBINED = "B"
    
    attr_accessor :value, :label

    def initialize(value)
      self.value = value
      @type = UNKNOWN
    end

    def type
      @type
    end

    def type=(type_val)
      raise "Cannot reset type from #{@type} to #{type_val}." unless @type == UNKNOWN || @type == type_val
      @type = type_val
    end

    def to_s
      "#{value}~#{label}~#{type}"
    end

    def clone
    	new_tok = Token.new(self.value)
    	new_tok.type = self.type
    	new_tok.label = self.label
    	new_tok
    end

  end
end