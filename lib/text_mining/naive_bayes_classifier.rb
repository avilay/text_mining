require 'benchmark'
require 'json'
require 'set'
require_relative 'text_probability_model'

module TextMining
  class NaiveBayesClassification    
    attr_accessor :category, :score

    def initialize(params)
      @category = params[:category]
      @score = params[:score]
    end
  end

  class NaiveBayesClassifier
    attr_accessor :model    

    def initialize
      @logger = Logger.new(nil)
      reset_state
    end

    def logger=(logr)
      @logger = logr
      @model.logger = logr
    end

    def reset_state
      # Hash of hashes counts = {'word' => {'CAT1' => 100, 'CAT2' => 10}}
      @counts = {}
      @cat_doc_counts = Hash.new(0)
      @cat_word_counts = Hash.new(0)
      @vocabulary = Set.new
      @model = TextProbabilityModel.new
      @model.logger = @logger
    end

    def clear
      @counts.clear
      @cat_doc_counts.clear
      @cat_word_counts.clear
      @vocabulary = nil
    end

    def pause_learn(state_file)
      @logger.debug('NaiveBayesClassifier.pause_learn') {"Pausing learn and saving state in #{state_file}"}

      File.open(state_file, 'w') do |file|
        file.puts(JSON.generate(@counts))
        @counts.clear

        file.puts(JSON.generate(@cat_doc_counts))
        @cat_doc_counts.clear

        file.puts(JSON.generate(@cat_word_counts))
        @cat_word_counts.clear

        file.puts(JSON.generate(@vocabulary.to_a))
        @vocabulary.clear
      end
    end

    def resume_learn(state_file)
      lines = IO.readlines(state_file)
      @counts = JSON.parse(lines[0])      
      @cat_doc_counts = JSON.parse(lines[1])
      @cat_word_counts = JSON.parse(lines[2])
      @vocabulary = Set.new(JSON.parse(lines[3]))  
    end

    # Any word that does not appear in at least 1% of the documents is chopped
    def chop_words(eta = 0.01)
      tot_instances = @cat_doc_counts.values.reduce(:+)

      @logger.info("NaiveBayesClassifier.chop_words") do 
        "Word count: #{@vocabulary.count}. Document count: #{tot_instances}. Chopping words that do not appear in #{eta*100}% of documents."
      end

      @vocabulary.each do |word|
        n = @counts[word]['num_instances'].to_f / tot_instances
        if n < eta
          @counts.delete(word)
          @vocabulary.delete(word)
        end
      end

      @logger.info("NaiveBayesClassifier.chop_words") do 
        "Word count after chopping: #{@vocabulary.count}."
      end

    end

    def begin_incremental_learn
      reset_state
    end

    def incremental_learn(instance)      
      @cat_word_counts[instance.category] += instance.document.words.count
      @vocabulary.merge(instance.document.words)
      @cat_doc_counts[instance.category] += 1
      instance.document.words.each do |word|
        (@counts[word] ||= Hash.new(0))[instance.category] += 1
      end
      instance.document.words.uniq.each do |uniq_word|
        @counts[uniq_word]['num_instances'] += 1
      end
    end

    def end_incremental_learn
      @logger.debug("NaiveBayesClassifier.end_incremental_learn") {"Ending incremental learn"}

      @model.vocabulary = @vocabulary
      @model.categories = @cat_doc_counts.keys

      # Set the categories and P(CAT)
      total_doc_count = @cat_doc_counts.values.reduce(:+)
      @cat_doc_counts.each do |category, cat_doc_count|
        pc = cat_doc_count.to_f/total_doc_count
        @model.probs.set_probability(of: category, value: pc)
      end

      # Set P(word/CAT)
      v_count = @model.vocabulary.count      
      ctr = 0
      @model.vocabulary.each do |word|
        @logger.info("NaiveBayesClassifier.end_incremental_learn"){"Processing word #{ctr}."} if ctr % 100000 == 0
        ctr += 1
        @model.categories.each do |category|
          tot_word_count_in_cat = @cat_word_counts[category]  
          word_count_in_cat = @counts[word][category] || 0
          p = (word_count_in_cat + 1).to_f / (tot_word_count_in_cat + v_count)
          @model.probs.set_probability(of: word, given: category, value: p)                  
        end
        @counts.delete(word)
      end      
    end

    def learn(instances)
      @logger.debug("NaiveBayesClassifier.learn") {"Starting to learn #{instances.count} number of instances"}

      @model = TextProbabilityModel.new
      @model.categories = instances.map {|i| i.category}.uniq
      instances.each do |instance|
        @model.vocabulary.merge(instance.document.words)
      end
      @model.categories.each do |category|
        docs = instances.find_all {|i| i.category == category}.map {|i| i.document}
        pc = docs.count.to_f / instances.count
        @model.probs.set_probability(of: category, value: pc)
        corpus = Document.unify_docs(docs)
        n = corpus.words.count
        @model.vocabulary.each do |word|
          n_k = corpus.frequency(word)
          p = (n_k + 1).to_f / (n + @model.vocabulary.count)
          @model.probs.set_probability(of: word, given: category, value: p)
        end
      end
    end

    def classify(doc)
      estimates = {}
      @model.categories.each do |category|
        estimate = @model.probs.probability(of: category)
        doc.words.each do |word|
          next unless @model.vocabulary.member?(word)
          estimate *= @model.probs.probability(of: word, given: category)
        end
        estimates[category] = estimate
      end
      est = estimates.max_by {|e| e[1]}
      NaiveBayesClassification.new(category: est[0], score: est[1])
    end

  end
end