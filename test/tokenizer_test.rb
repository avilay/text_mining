require 'test/unit'
require 'logger'
require_relative '../lib/text_mining/tokenizer'

include TextMining

class TokenizerTest < Test::Unit::TestCase
  def setup
    @tokenizer = Tokenizer.new
  end

  def test_stemming
    @tokenizer.configure(use_stemming: "yes")
    expected_tokens = %w[thi sentanc full stopword]
    actual_tokens = @tokenizer.tokenize('This is a sentance Full of Stopwords').words
    assert_equal(expected_tokens, actual_tokens)
  end

  def test_tokenize
    logger = Logger.new(STDOUT)
    logger.level = Logger::ERROR

    @tokenizer.configure(use_stemming: "no", stopwords: {}, logger: logger)

    actual_tokens = @tokenizer.tokenize('is 1.5m. USD not Sw F. by A.P. initials. (*): i').words
    expected_tokens = ["is", "1.5", "m", ".", "usd", "not", "sw", "F.", "by", "A.P.", "initials", ".", "(*): i"]
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('my name is avilay :-)').words
    expected_tokens = ["my", "name", "is", "avilay", ":-)"] 
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('hello/world 5M., from AT&T co-operation Feb 28th, 05').words
    expected_tokens = ["hello/world", "5", "M.", ",", "from", "AT&T", "co-operation", "feb", "28th", ",", "05"]
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('my email is avilay@gmail.com and my blog is at http://avilay.wordpress.com and of course there is no place like 127.0.0.1 :-)').words
    expected_tokens = ["my", "email", "is", "avilay@gmail.com", "and", "my", "blog", "is", "at", "http://avilay.wordpress.com", "and", "of", "course", "there", "is", "no", "place", "like", "127.0.0.1", ":-)"]
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('GDP is up 14% this month with error +2.5 to -2.5. The total value is 1,500,345 million. Estimates of -1.54E3 have also been given.').words
    expected_tokens = ["gdp", "is", "up", "14%", "this", "month", "with", "error", "+2.5", "to", "-2.5", ".", "the", "total", "value", "is", "1,500,345", "million", ".", "estimates", "of", "-1.54E3", "have", "also", "been", "given", "."]
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('hello,   world[0]').words
    expected_tokens = %w(hello , world [ 0 ])
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('<code> this is some code </code>').words
    expected_tokens = %w(<code> this is some code </code>)
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('access (S.M.A.R.T.)(http://www.microsoft.com)').words
    expected_tokens = %w[access ( S.M.A.R.T. )( http://www.microsoft.com )]
    assert_equal(expected_tokens, actual_tokens)

    actual_tokens = @tokenizer.tokenize('random F.live.com link').words
    expected_tokens = %w[random F. live.com link]
    assert_equal(expected_tokens, actual_tokens)

  end
end