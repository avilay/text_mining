require 'test/unit'
require_relative '../lib/text_mining/naive_bayes_classifier'
require_relative '../lib/text_mining/text_probability_model'
require_relative '../lib/text_mining/document_instance'
require_relative '../lib/text_mining/tokenizer'

=begin
This is a test class for NaiveBayesTest and is intended
to contain all NaiveBayesTest Unit Tests
Test example set:
 "yellow yellow yellow yellow red" --> like
 "yellow yellow red" --> like
 "yellow yellow blue blue" --> like
 "yellow yellow blue blue" --> dislike
 "yellow yellow blue blue blue blue" --> dislike
 "yellow yellow blue blue blue blue blue" --> dislike
 This results in the following table:
            +------+---------+
            | LIKE | DISLIKE |
            +------+---------+
     yellow |  8   |    6    |
            +------+---------+
     blue   |  2   |    11   |
            +------+---------+
     red    |  2   |    0    |
            +------+---------+
               12       17

 This in turn results in the following probability table
            +------+---------+
            | LIKE | DISLIKE |
            +------+---------+
     yellow | 0.6  |  0.35   |
            +------+---------+
     blue   | 0.2  |  0.6    |
            +------+---------+
     red    | 0.2  |  0.05   |
            +------+---------+

 p(yellow/like) = (n_yellow + 1) / (n + |v|)
 n_yellow = number of times "yellow" occurs in LIKE documents, 8 in this case.
 n = number of words in LIKE documents, 12 in this case
 |v| = size of vocuabulary, 3 in this case
 p(yellow/like) = (8+1)/(12+3) = 0.6
 refer to test_data.xlsx for some more number crunching

 test data:
 for p(like) = 0.5 and p(dislike) = 0.5
 "yellow yellow blue red" --> e(like/unknown) = 0.0072, e(dislike/unknown) = 0.0018375
  
  p(like/unknown) = p(like & unknown)/p(unknown) = {p(like).p(unknown/like)}/p(unknown)
  p(dislike/uknown) = .... = {p(dislike).p(unknown/dislike)}/p(unknown)

  e(like/unknown) = p(like).p(unknown/like) = 0.5*p(y/l)*p(y/l)*p(b/l)*p(r/l) = 0.5*0.6*0.6*0.2*0.2 = 0.0072

 "blue red" --> p(like/unknown) = 0.02, p(dislike/unknown) = 0.015
 "yellow yellow blue red green" --> same as "yellow yellow blue red". "green" should be ignored as it was not part of original vocabulary.
 "green" --> p(like/unknown) = p(like) = 0.5, p(dislike/unknown) = p(dislike) = 0.5
 "" --> same as "green"
=end

include TextMining

class NbClassifierTest < Test::Unit::TestCase

  def setup
    @tk = Tokenizer.new
    
    doc1 = @tk.tokenize("yellow yellow yellow yellow red")
    di1 = DocumentInstance.new(doc1, 'like')

    doc2 = @tk.tokenize("yellow yellow red")
    di2 = DocumentInstance.new(doc2, 'like')

    doc3 = @tk.tokenize("yellow yellow blue blue")
    di3 = DocumentInstance.new(doc3, 'like')

    doc4 = @tk.tokenize("yellow yellow blue blue")
    di4 = DocumentInstance.new(doc4, 'dislike')

    doc5 = @tk.tokenize("yellow yellow blue blue blue blue")
    di5 = DocumentInstance.new(doc5, 'dislike')

    doc6 = @tk.tokenize("yellow yellow blue blue blue blue blue")
    di6 = DocumentInstance.new(doc6, 'dislike')

    @instances = [di1, di2, di3, di4, di5, di6]
    @nbc = NaiveBayesClassifier.new
  end

  def test_incremental_learn
    @nbc.begin_incremental_learn
    @instances.each {|i| @nbc.incremental_learn(i)}
    @nbc.end_incremental_learn
    learned_model_asserts(@nbc.model)
  end

  def learned_model_asserts(nbm)
    assert_in_delta(0.5, nbm.probs.probability(of: 'like'), 0.01)
    assert_in_delta(0.5, nbm.probs.probability(of: 'dislike'), 0.01)

    ["yellow", "red", "blue"].each do |v|
      assert(nbm.vocabulary.member?(v))
    end

    like_probs = {'yellow' => 0.6, 'blue' => 0.2, 'red' => 0.2}
    dislike_probs = {'yellow' => 0.35, 'blue' => 0.6, 'red' => 0.05}
    like_probs.each do |word, prob|
      expected_prob = prob
      actual_prob = nbm.probs.probability(of: word, given: 'like')
      assert_equal(expected_prob, actual_prob)
    end
    dislike_probs.each do |word, prob|
      expected_prob = prob
      actual_prob = nbm.probs.probability(of: word, given: 'dislike')
      assert_equal(expected_prob, actual_prob)
    end
  end

  def test_pause_resume
    @nbc.begin_incremental_learn
    @instances.each {|i| @nbc.incremental_learn(i)}
    @nbc.pause_learn('nbc_state.txt')
    nbc2 = NaiveBayesClassifier.new
    nbc2.resume_learn('nbc_state.txt')
    nbc2.end_incremental_learn
    learned_model_asserts(nbc2.model)
    File.delete('nbc_state.txt')
  end

  def test_learn
    @nbc.learn(@instances)
    learned_model_asserts(@nbc.model)
  end

  def test_classify
    @nbc.learn(@instances)
    unknown_doc_asserts(@nbc)

    nbc2 = NaiveBayesClassifier.new
    nbc2.model = @nbc.model
    unknown_doc_asserts(nbc2)
  end

  def unknown_doc_asserts(nbc)
    doc = @tk.tokenize("yellow yellow blue red")
    cat = nbc.classify(doc)
    assert_equal('like', cat.category)
    assert_in_delta(0.0072, cat.score, 0.001)

    doc = @tk.tokenize("blue red")
    cat = nbc.classify(doc)
    assert_equal('like', cat.category)
    assert_in_delta(0.02, cat.score, 0.001)

    doc = @tk.tokenize("yellow yellow blue red green")
    cat = nbc.classify(doc)
    assert_equal('like', cat.category)
    assert_in_delta(0.0072, cat.score, 0.001)

    doc = @tk.tokenize("green")
    cat = nbc.classify(doc)
    assert_in_delta(0.5, cat.score, 0.001)

    doc = @tk.tokenize("")
    cat = nbc.classify(doc)
    assert_in_delta(0.5, cat.score, 0.001)
  end
end