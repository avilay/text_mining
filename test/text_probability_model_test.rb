require 'test/unit'
require_relative '../lib/text_mining'

include TextMining

class TextProbabilityModelTest < Test::Unit::TestCase

  def setup
    tk = Tokenizer.new
    
    doc1 = tk.tokenize("yellow yellow yellow yellow red")
    di1 = DocumentInstance.new(doc1, 'like')

    doc2 = tk.tokenize("yellow yellow red")
    di2 = DocumentInstance.new(doc2, 'like')

    doc3 = tk.tokenize("yellow yellow blue blue")
    di3 = DocumentInstance.new(doc3, 'like')

    doc4 = tk.tokenize("yellow yellow blue blue")
    di4 = DocumentInstance.new(doc4, 'dislike')

    doc5 = tk.tokenize("yellow yellow blue blue blue blue")
    di5 = DocumentInstance.new(doc5, 'dislike')

    doc6 = tk.tokenize("yellow yellow blue blue blue blue blue")
    di6 = DocumentInstance.new(doc6, 'dislike')

    instances = [di1, di2, di3, di4, di5, di6]
    nbc = NaiveBayesClassifier.new
    @tpm = nbc.learn(instances)
  end

  def test_entropy
    print "Running test_entropy: "
    @tpm.calculate_entropies
    expected_entropies = {'' => 1.0, 'yellow' => 0.95, 'blue' => 0.81, 'red' => 0.72}
    @tpm.entropies.each do |word, actual_entropy|
      assert_in_delta(expected_entropies[word], actual_entropy, 0.01)
    end
    print "PASS\n"
  end

  def test_info_gains
    print "Running test_info_gains: "
    @tpm.calculate_info_gains
    yellow_gain = InfoGain.new(feature: 'yellow', category: 'like', value: 0.05)
    blue_gain = InfoGain.new(feature: 'blue', category: 'dislike', value: 0.19)
    red_gain = InfoGain.new(feature: 'red', category: 'like', value: 0.28)
    assert_equal(3, @tpm.info_gains.count)
    
    # Test yellow gain
    actual_yellow_gain = @tpm.info_gains.find {|ig| ig.feature == 'yellow'}
    refute_nil(actual_yellow_gain)
    assert_equal(yellow_gain, actual_yellow_gain)

    # Test blue gain
    actual_blue_gain = @tpm.info_gains.find {|ig| ig.feature == 'blue'}
    refute_nil(actual_blue_gain)
    assert_equal(blue_gain, actual_blue_gain)

    # Test red gain
    actual_red_gain = @tpm.info_gains.find {|ig| ig.feature == 'red'}
    refute_nil(actual_red_gain)
    assert_equal(red_gain, actual_red_gain)
    print "PASS\n"
  end

end